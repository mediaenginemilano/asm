﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace ASM
{
	public class WebServiceManager
	{
		IRestService restService;

		public WebServiceManager (IRestService service )
		{
			restService = service;

		}

	

		public Task<List<ItemNews>>GetDataXml(string str)
		{
			return restService.GetDataXml( str);	
		}


		public Task<bool> TrackPage (GoogleTrack hitbox){


			return restService.TrackPage (hitbox);
		}


		public Task<string> Get_Value_File (string Key){
		
			return restService.Get_Value_File (Key);
		}


		public  Task Save_Value(string Key, string ValueToSave){

			return restService.Save_Value (Key,  ValueToSave);
		}


		public Task Delete_Value (string Key){
		
			return restService.Delete_Value (Key);
		}

		public Task<string> SendDataToWeb (List <Controls_front> Json ){

			return restService.SendDataToWeb  (Json);
		}




	}
}
