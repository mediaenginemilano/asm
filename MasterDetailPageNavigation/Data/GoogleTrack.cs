﻿using System;
using Newtonsoft.Json;

namespace ASM
{
	public class GoogleTrack
	{


		[JsonProperty("tid")]
		public string tid { get; set; }
		[JsonProperty("cid")]
		public string cid { get; set; }
		[JsonProperty("dp")]
		public string dp { get; set; }

		[JsonProperty("v")]
		public string v { get; set; }

		[JsonProperty("t")]
		public string t { get; set; }
	}
}

