﻿using System;
using System.Collections.Generic;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System.IO;
using System.Diagnostics;
using System.Threading.Tasks;
using System.Xml.Linq;
using System.Xml.Serialization;
using System.Linq;
using PCLStorage;

namespace ASM
{
	public class DataSurvey
	{

	
		public static List <Controls_front> SData = new List<Controls_front> ();

		public  List <Controls_front> GetSData { get { return SData; } }




		public static bool thereisFile = false;

		public  bool checkFile  { get { return thereisFile; } }

		//	public  bool GetSData { get { return SData; } }
	

		public DataSurvey ()
		{

			GetDataSurvey ();

		}


		public  async void  GetDataSurvey ()
		{


			var f = await App.ServiceManager.Get_Value_File ("f");


			if (f.Equals ("errore")) {


			


			} else if (f.Equals ("nofile")) {

				//Debug.WriteLine ("no file");


				/*
				var j=JsonConvert.SerializeObject (SData);
				await App.ServiceManager.Save_Value ("f",j);
*/

			} else {

			
		

				//Debug.WriteLine ("cè file");


				//SDATA as now syc to file

				SData = JsonConvert.DeserializeObject <List<Controls_front>> (f);
				thereisFile = true;

				//var current= account.Where(p => p.nome == "s1_ck_2");

				//	SetDataSurvey ("s2_rd_2", "1", "s2_b") ;



			}




			
		}



		public void DeleteResultFile(){


			App.ServiceManager.Delete_Value ("f");
			thereisFile =false;
			SData = new List<Controls_front> ();



		}


		public void SaveResultFile(){

			var j=JsonConvert.SerializeObject (SData);
			App.ServiceManager.Save_Value ("f",j);

			thereisFile =true;


		}



	
		public  string[] GetDataGroup (string gruppo)
		{


			var elementi = SData.Where (p => p.gruppo == gruppo);
			string[] tohide = new string[elementi.Count ()];

			var i = 0;

			foreach (var item in elementi) {
			
				tohide [i] = item.nome;

				i++;
			}


			return tohide;

		}







		public  void  AddDataSurvey (Controls_front el)
		{

		
			SData.Add (el);

		}




		public  Controls_front  GetElByName (string  name)
		{


			var e = SData.Where (p => p.nome == name);

			var cf = e.ElementAt (0);


			return cf;

				

		}


		public  string  GetParentBySon (string son)
		{

			var elementi = SData.Where (p => p.son != null);
			string parent = "";

			foreach (var item in elementi) {
			
				if (item.son.IndexOf (son) > -1) {

					parent = item.nome;
					break;
				}

			}


			return  parent;

		}






		public  void  SetScoreSurvey (string nome, int score)
		{


			var elemento = SData.Where (p => p.nome == nome);
			foreach (var item in elemento) {
				//Debug.WriteLine ("add score  " + score.ToString ());
				item.score = score;
			}


		}



		public  void  SetSonSurvey (string nome, string index)
		{


			var elemento = SData.Where (p => p.nome == nome);
			foreach (var item in elemento) {

				item.son = index;
			}


		}




		public  void  SetTextSurvey (string nome, string txt)
		{


			var elemento = SData.Where (p => p.nome == nome);
			foreach (var item in elemento) {
				//Debug.WriteLine ("add txt  " + txt);
				item.txt = txt;
			}


		}




		public  void  getRelationVal(){

			var r = "0";


			var precedente = SData.Where (p =>  (p.nome == "s6_rd_3")   ).FirstOrDefault();
			var corrente = SData.Where (p =>  (p.nome == "s7_rd_1")   ).FirstOrDefault();

			if (precedente != null) {


				if (!precedente.valore.Equals ("1")) {

					r = "1";
				} 



			}


			if (corrente != null) {
				

				if (  precedente.valore.Equals ("1")     ) {
				
					corrente.score = 0;
				
				}

				//corrente.score =  r=="1"?r:"";
				//corrente.score = r?"1":"";

				//corrente.valore=r=="1"?r:"";
				//corrente.score=r=="1"?1:0;
			}


			//return r;
		}



		public void fixRelation(string nome, string valore){

			//s6_rd_3
			//s7_rd_1


			var  corrente="";

			var  nuovo_valore=0;


		


			if (nome.Equals ("s6_rd_3") && valore.Equals ("1")) {
			
				corrente ="s7_rd_1";
				nuovo_valore=0;
			
			}


			if (nome.Equals ("s6_rd_2")      ) {
			   
				corrente ="s7_rd_1";
				nuovo_valore=1;

			
			}



			if (nome.Equals ("s6_rd_1")    ) {

				corrente ="s7_rd_1";
				nuovo_valore=0;


			}


		




			if (nome.Equals ("s7_rd_1") ) {

				var precedente = SData.Where  ( p =>  (p.nome == "s6_rd_3") ).FirstOrDefault();

				if (precedente != null) {
				
					if (precedente.valore.Equals("1")){

						corrente ="s7_rd_1";
						nuovo_valore=0;
					}
				}

				var precedente2 = SData.Where  ( p =>  (p.nome == "s6_rd_1") ).FirstOrDefault();

				if (precedente2 != null) {

					if (precedente2.valore.Equals("1")){

						corrente ="s7_rd_1";
						nuovo_valore=0;
					}
				}




			}









			if ( !corrente.Equals("") ){

				var azzero = SData.Where ( p => (p.nome == corrente)  ).FirstOrDefault();

				if (azzero!=null){

					azzero.score=nuovo_valore;
					Debug.WriteLine ("Corrente " + nome +" | Corrente valore " + valore  +  " | Azzero  " + azzero.nome + " | Azzero score "+ azzero.score );
				}
			

			}










		}



		public  void  SetDataSurvey (string nome, string valore, string gruppo)
		{


			if (!gruppo.Equals ("")) {


				var elementi = SData.Where (p => p.gruppo == gruppo);

				foreach (var item in elementi) {

					item.valore = "";
					 
					if (item.nome.Equals (nome)) {
					

						//Debug.WriteLine ("add val" + valore);
						item.valore = valore;

					}
				
				}



			} else {



			

				var elemento = SData.Where (p => p.nome == nome);
				foreach (var item in elemento) {
					//Debug.WriteLine ("add val sing " + valore);
					item.valore = valore;
				}
			

				//	elemento.valore=
			
			
			}


			////Debug.WriteLine ("ADDED Nome " + nome + " Valore  " +valore  );


			////Debug.WriteLine ("Check  valore" + (SData.Where (p => p.nome == nome && p.valore == valore)).First().valore);

			//var item = SData.Where(p => p.nome == nome);


			Debug.WriteLine(JsonConvert.SerializeObject (SData));

			fixRelation (nome, valore);


		}




		public  int  GetTotScoreBSurvey ()
		{


			int total = 0;


			var elementi = GetSData.Where (p =>    p.valore.Equals ("1")    );





			foreach (var item in elementi) {

				//Debug.WriteLine ("Nome " + item.nome + " Valore  " +item.valore + " Score  " +item.score);

				if (item.scoreB==(int)item.scoreB ) {

					Debug.WriteLine ("Nome " + item.nome + " Valore  " +item.valore + " Score  " +item.scoreB);
					total += item.scoreB;
				}


			}


			return total;


		}




		public  int  GetTotScoreSurvey ()
		{


			int total = 0;


			var elementi = SData.Where (p =>  p.valore.Equals ("1") );

			foreach (var item in elementi) {



				if (item.score==(int)item.score  ){//if (!item.valore.Equals ("") && !item.valore.Equals (null)) {


					total += item.score;


					Debug.WriteLine ("Nome " + item.nome + " Valore  " +item.valore + " TOT  " +total);
				}


			}


			return total;


		}


		public  bool GetIsPageCompleted (string nomePagina)
		{


			bool total = true;


			var elementi = SData.Where (p => p.pagina == nomePagina);

			//var domanda=0

			foreach (var item in elementi) {


				//Debug.WriteLine ("Valore " + item.nome + "  " +item.valore);


				var valore = item.valore.Equals ("") || item.valore.Equals (null) ? false : true;

				switch (item.tipo) {


				case "label":


					Debug.WriteLine ("LABEL VALORE REQ " + item.isReq);
					
					if (!valore) {


						total = false;

						if(item.isReq==1){

							total = true;

						}



						break;
					}


					break;
				case "sel":	

					if (!valore) {

					
						total = false;

						if(item.isReq==1){

							total = true;

						}




						break;
					}


					break;

				case "check":
					
					if (!valore) {

						//check su padre, se il padre è checked , almeno un fratello lo sarà.

			

						var bro_ =	elementi.Where (p => (p.gruppo == item.gruppo && p.valore.Equals ("1")));


						var isok = true;


				/*		if (item.son != null && item.valore.Equals ("1")) {


							string[] valori = item.son.Split ( new char[] { ',' });

							foreach (var sub in valori) {
							
								 
								if (!GetElByName (sub).valore.Equals ("1")){

									isok = false;
									total = false;
									break;

								}
							
							
							}


							// GetElByName (string  name)




						}

*/
					/*	foreach (var elm in  bro_) {

						

							if (elm.son!=null) {

								string[] valori = elm.son.Split ( new char[] { ',' });


								foreach (var sub in valori) {


									var sons =	elementi.Where (p => (p.nome == sub   && p.valore.Equals ("1")));

									if (sons.Count () <= 0 ) {

										isok = false;
										total = false;
										break;
										//Debug.WriteLine ("Manca valore di " + item.nome);

									}


								}


							



							}

						}
				*/

						if (!isok){

							break;
						}


						if (bro_.Count () <= 0) {


							total = false;
							break;
							//Debug.WriteLine ("Manca valore di " + item.nome);

						} 




						
			




					
					/*		var padre =	GetParentBySon (item.nome);

						var parent = elementi.Where (p => p.nome == padre);
					



						if (parent.Count () > 0) {

							 var bro =	elementi.Where (p => (p.gruppo == parent.First ().gruppo && p.valore.Equals ("1")));








							if (!parent.First ().valore.Equals ("1") && bro.Count () <= 0) {

								//Debug.WriteLine ("Manca valore padre di " + item.nome);
								total = false;
								break;
							}




						} 

						*/


							

					} else {
					
					//	var children =	elementi.Where (p => (p.gruppo == item.gruppo && p.nome!=item.nome && p.valore.Equals ("1")));

					var isok = 0;
					if (item.son != null && item.valore.Equals ("1")) {


						string[] valori = item.son.Split ( new char[] { ',' });

						foreach (var sub in valori) {


							if (GetElByName (sub).valore.Equals ("1")){

								isok ++;
								//break;


							}


						}


						if (isok==0){

							total = false;
							break;
						}



						// GetElByName (string  name)




					}


							
					/*	if ((children.Count () <= 0) && (item.son != null)) {


							total = false;
							break;
							//Debug.WriteLine ("Manca valore di " + item.nome);

						} */
					
					}



					break;
					

				case "radio":

					if (!valore) {


						var padre =	GetParentBySon (item.nome);

						var parent = elementi.Where (p => p.nome == padre);


						if (parent.Count () <= 0) {

							//check su gruppo.
							var bro =	elementi.Where (p => (p.gruppo == item.gruppo && p.valore.Equals ("1")));


							if (bro.Count () <= 0) {
								//Debug.WriteLine ("Manca valore di un elemento del gruppo di " + item.nome);

								total = false;
								break;

							} else {
							
							
								//Debug.WriteLine ("****>" + item.nome);

							
							
							
							}


						
						} else {
						



							var bro =	elementi.Where (p => (p.gruppo == parent.First ().gruppo && p.valore.Equals ("1")));

							if (bro.Count () <= 0) {

								//Debug.WriteLine ("Manca valore di un elemento di secondo livello del gruppo di " + item.nome);
								total = false;
								break;
							
							} else {



								var bro_ =	elementi.Where (p => (p.gruppo == item.gruppo && p.valore.Equals ("1")));




								if (bro_.Count () <= 0 && parent.First ().valore.Equals ("1")) {

									//Debug.WriteLine ("Manca valore XXX " + item.nome);
									total = false;
									break;

								}





							}

						



						
						}



					


						
					
					}




					break;


				}




			}


			return total;


		}


		/*public async Task<T> Get_Value<T>(String Key)
		{
			IFolder rootFolder = FileSystem.Current.LocalStorage;
			IFolder folder = await rootFolder.CreateFolderAsync("Cache",CreationCollisionOption.OpenIfExists);

			ExistenceCheckResult isFileExisting = await folder.CheckExistsAsync(Key + ".json");

			if (!isFileExisting.ToString ().Equals ("NotFound")) {
				try {
					IFile file = await folder.CreateFileAsync (Key + ".json", CreationCollisionOption.OpenIfExists);

					String languageString = await file.ReadAllTextAsync ();

					//XmlSerializer oXmlSerializer = new XmlSerializer(typeof(T));

				//	JsonTextReader reader = new JsonTextReader (languageString);
						
				//	JObject o2 = (JObject)JToken.ReadFrom (reader);


					//return  (T)o2;
					//return (T)oXmlSerializer.Deserialize(new StringReader(languageString));
				} catch (Exception ex) {

					////Debug.WriteLine ("noFILE!");
					return default(T);
				}
			} else {
			
				//Debug.WriteLine ("noFILE!");
			
			}

			return default(T);
		}*/

		/// <summary>
		/// Delete any value from the Storage...
		/// </summary>
		/// <param name="Key"></param>



	

	}




}
