﻿using System;

namespace ASM
{
	public class Controls_front
	{
		public string pagina{ get; set; }
		public string nome{ get; set; }
		public string valore{ get; set; }
		public string tipo{ get; set; }
		public string gruppo{ get; set; }
		public int score{ get; set; }
		public int scoreB{ get; set; }
		public string son { get; set;}
		public string txt { get; set;}
		public int isReq{ get; set; }
	}
}

