﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;

using Newtonsoft.Json;
using System.Xml.Linq;
using System.Linq;
using PCLStorage;

using System.Text.RegularExpressions;

namespace ASM
{
	public class RestService : IRestService
	{
		HttpClient client;

		public List<ItemNews> Items { get; private set; }


		public RestService ()
		{
			var authData = string.Format ("{0}:{1}", "FLO", "a123456");
			var authHeaderValue = Convert.ToBase64String (Encoding.UTF8.GetBytes (authData));

			client = new HttpClient ();
			//client.MaxResponseContentBufferSize = 256000;
			client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue ("Basic", authHeaderValue);
		}





		public async Task<string> SendDataToWeb (List <Controls_front> Json )
		{
			var uri = new Uri (string.Format ("http://dev.mediaengine.it/ASM/post.php", string.Empty));
			var result = "";

			var jsonToSend =JsonConvert.SerializeObject (Json);

			//"{ \"username\":"+ user.email +",\"password\":"+ user.password  +" }";

			//





			var content = new StringContent (jsonToSend, Encoding.UTF8, "application/json");


		

			try {

				HttpResponseMessage response = await client.PostAsync (uri, content);

				if (response.IsSuccessStatusCode) {

					result =   response.ReasonPhrase;

						//Content.ReadAsStringAsync ();


				} else {

					//pagina non trovata
					result  = @"{'codicestato': '4','descrizione':'" + response.StatusCode.ToString () + "'}";

				}


				//Status converted = JsonConvert.DeserializeObject<Status> (json);

				return result ;


			} catch (Exception ex) {
				//Debug.WriteLine (@"				ERROR {0}", ex.Message);
			}



			return result ;

		}



		public async Task<bool>  TrackPage (GoogleTrack hitbox )
		{


			var uri = new Uri (string.Format ("https://www.google-analytics.com/collect", string.Empty));
			var result = false;
		//	var jsonToSend =JsonConvert.SerializeObject (hitbox);
		//	var content = new StringContent (jsonToSend, Encoding.UTF8, "x-www-form-urlencoded");


			var postData = new List<KeyValuePair<string, string>> ();
			postData.Add (new KeyValuePair<string, string> ("v", hitbox.v));
			postData.Add (new KeyValuePair<string, string> ("t", hitbox.t));
			postData.Add (new KeyValuePair<string, string> ("tid", hitbox.tid));
			postData.Add (new KeyValuePair<string, string> ("cid", hitbox.cid));
			postData.Add (new KeyValuePair<string, string> ("dp", hitbox.dp));





			var content = new FormUrlEncodedContent (postData);





			try {

				HttpResponseMessage response = await client.PostAsync (uri, content);

				if (response.IsSuccessStatusCode) {

					result = true;


					//json = await response.Content.ReadAsStringAsync ();


				} else {

					//pagina non trovata
				//	json = @"{'codicestato': '4','descrizione':'" + response.StatusCode.ToString () + "'}";

				}


				//Status converted = JsonConvert.DeserializeObject<Status> (json);

				return result;


			} catch (Exception ex) {
				//Debug.WriteLine (@"				ERROR {0}", ex.Message);

				return result;
			}

			return result;




		}



		public  async Task<string> Get_Value_File (string Key)
		{
			IFolder rootFolder = FileSystem.Current.LocalStorage;
			IFolder folder = await rootFolder.CreateFolderAsync("Cache",CreationCollisionOption.OpenIfExists);

			ExistenceCheckResult isFileExisting = await folder.CheckExistsAsync(Key + ".json");

			var rt="";


			if (!isFileExisting.ToString ().Equals ("NotFound")) {
				try {
					IFile file = await folder.CreateFileAsync (Key + ".json", CreationCollisionOption.OpenIfExists);

					rt = await file.ReadAllTextAsync ();

					//XmlSerializer oXmlSerializer = new XmlSerializer(typeof(T));

					//	JsonTextReader reader = new JsonTextReader (languageString);

					//	JObject o2 = (JObject)JToken.ReadFrom (reader);

					return rt;

					//return  (T)o2;
					//return (T)oXmlSerializer.Deserialize(new StringReader(languageString));
				} catch (Exception ex) {

					//Debug.WriteLine ("errore" + ex);
					rt = "errore";
					return rt;
				}

			} else {

				//Debug.WriteLine ("noFILE!");

				rt = "nofile";

			}





			return rt;
		}


		public async Task Save_Value(string Key, string ValueToSave)
		{
			

			IFolder rootFolder = FileSystem.Current.LocalStorage;
			IFolder folder = await rootFolder.CreateFolderAsync("Cache",
				CreationCollisionOption.OpenIfExists);
			IFile file = await folder.CreateFileAsync(Key + ".json",
				CreationCollisionOption.ReplaceExisting);

			await file.WriteAllTextAsync(ValueToSave);
		}


		public async Task Delete_Value (string Key)
		{
			IFolder rootFolder = FileSystem.Current.LocalStorage;
			IFolder folder = await rootFolder.CreateFolderAsync ("Cache",
				CreationCollisionOption.OpenIfExists);

			ExistenceCheckResult isFileExisting = await folder.CheckExistsAsync (Key +  ".json");

			if (!isFileExisting.ToString ().Equals ("NotFound")) {
				try {
					IFile file = await folder.CreateFileAsync (Key +  ".json",
						CreationCollisionOption.OpenIfExists);

					await file.DeleteAsync ();
				} catch (Exception ex) {

				}
			}
		}



		public async Task <List<ItemNews>>GetDataXml (string str=""){

			string content;


			if (str.Equals("") ){

				str = "http://asmonlus.blogspot.com/feeds/posts/default?alt=rss";
			}


			Items = new List<ItemNews> ();

			// Feed generico del blog
			var uri = new Uri (string.Format (str, string.Empty));

			// Feed per News (categoria del blog)
			// var uri = new Uri (string.Format ("http://asmonlus.blogspot.it/feeds/posts/default/-/News/?alt=rss", string.Empty));

			// Feed per Ricette (categoria del blog)
			// var uri = new Uri (string.Format ("http://asmonlus.blogspot.it/feeds/posts/default/-/Ricette/?alt=rss", string.Empty));


			try {
				HttpResponseMessage  response = await client.GetAsync (uri);
				if (response.IsSuccessStatusCode) {
					content = await response.Content.ReadAsStringAsync ();

					XDocument xdocument = XDocument.Parse(content);

					var allItems = xdocument.Descendants("item").Select(x => new {
						Title = x.Element("title").Value,
						Description= x.Element("description").Value,

						Url= x.Element("link").Value

					});

					//IEnumerable<XElement> employees = xdocument.Elements();
					foreach (var item in  allItems )
					{

					//	HtmlDocument doc = new HtmlDocument();
					//	doc.LoadHtml(item.Description.ToString());

						string matchString = Regex.Match( System.Net.WebUtility.HtmlDecode (item.Description.ToString()), "<img.+?src=[\"'](.+?)[\"'].+?>", RegexOptions.IgnoreCase).Groups[1].Value;

						var lnk=item.Url;


						if (matchString.Length>0){

							lnk= Regex.Match( System.Net.WebUtility.HtmlDecode (item.Description.ToString()), "<img.+?alt=[\"'](.+?)[\"'].+?>", RegexOptions.IgnoreCase).Groups[1].Value;

							
						}

						if (lnk.Length<=0){

							lnk="http://www.asmonlus.it/NEWS.aspx";
						}


						Items.Add (new ItemNews {
							Title = item.Title ,
							IconSource= matchString,
							Url=lnk
						});


					 


						//employee.Element("Name").Value
						Debug.WriteLine("-->" + matchString );
					}


			



				}else{

					content = "No page";
					//pagina non trovata


				}
			} catch (Exception ex) {

				//errore
				content = "errore";
				//Debug.WriteLine (@"				ERROR {0}", ex.Message);
			}

			return   Items;


		}



	}
}