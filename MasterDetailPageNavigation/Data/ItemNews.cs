﻿using System;



namespace ASM
{
	public class ItemNews
	{
		public string Title { get; set; }

		public string IconSource { get; set; }

		public string Description  { get; set; }

		public string Url { get; set; }



	}
}
