﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;


namespace ASM
{
	public interface IRestService
	{
		Task<List<ItemNews>> GetDataXml (string str);

		Task<bool> TrackPage (GoogleTrack hitbox);

		Task<string> Get_Value_File (string Key);


		Task Save_Value (string Key, string ValueToSave);


		Task Delete_Value (string Key);


		Task<string> SendDataToWeb (List <Controls_front> Json);


	}
}
