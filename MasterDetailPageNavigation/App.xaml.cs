﻿using System;
using System.Collections.Generic;

using Xamarin.Forms;
using System.Globalization;

using System.Reflection;
using System.Diagnostics;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

using System.Linq;
using System.Xml.Linq;

namespace ASM
{
	public partial class App : Application
	{
		public static WebServiceManager ServiceManager { get; private set; }
		public static DataSurvey Survey { get; private set; }


		public static string forcedCulture="";
		public static GoogleTrack hitbox;



		public static Page getCurrentPage { get { return    Current.MainPage;} }

	
		public App ()
		{

			ServiceManager = new WebServiceManager (new RestService ());

			System.Diagnostics.Debug.WriteLine("===============");
			var assembly = typeof(App).GetTypeInfo().Assembly;
			foreach (var res in assembly.GetManifestResourceNames()) 
				System.Diagnostics.Debug.WriteLine("found resource: " + res);


			if (Device.OS != TargetPlatform.WinPhone) {
				DependencyService.Get<ILocalize> ().SetLocale ();
				//Resx.AppResources.Culture = DependencyService.Get<ILocalize>().GetCurrentCultureInfo();
			}

			InitializeComponent ();


			MainPage = new ASM.MainPage () ; // change as required
		



			// The Application ResourceDictionary is available in Xamarin.Forms 1.3 and later
			if (Application.Current.Resources == null) {
				Application.Current.Resources = new ResourceDictionary();
			}

			var appStyle = new Style (typeof(Label)) {
				BaseResourceKey = Device.Styles.SubtitleStyleKey,
				Setters = {
					new Setter { Property = Label.TextColorProperty, Value = Color.Green }
				}
			};

			Application.Current.Resources.Add ("AppStyle", appStyle);

			var boxStyle = new Style (typeof(BoxView)) {
				Setters = {
					new Setter { Property = BoxView.ColorProperty, Value = Color.Aqua }
				}
			};
			Application.Current.Resources.Add (boxStyle); // implicit style for ALL boxviews



			//init google

			hitbox = new GoogleTrack ();
			hitbox.cid="9c6e2ae0ff46cfc2ef240fb28e876eb6272b8148";
			hitbox.tid = "UA-73445738-1";
			hitbox.t="pageview";
			hitbox.v = "1";



			Survey = new DataSurvey ();

			var json=  (JsonConvert.SerializeObject (Survey.GetSData));

			//Debug.WriteLine (json);

			//ParseFiles(ASM.re

			/*var postTitles = json["nome"] ; 


			//Debug.WriteLine (json);

			foreach (string i in postTitles)
				{
				//Debug.WriteLine(i);
				}

*/





			////Debug.WriteLine (f);

			//List<string> videogames = JsonConvert.DeserializeObject<List<string>>(json);


		//	hitbox.dp="home page";

		//	ServiceManager.TrackPage (hitbox);






		}






	}
}