﻿using System;
using Xamarin.Forms;
using System.Diagnostics;
using Newtonsoft.Json;

namespace ASM
{
	public class Controls_me: StackLayout
	{
		public Controls_me ()
		{

			var tgr = new TapGestureRecognizer ();
			tgr.Tapped += (s, e) => OnLabelClicked ();
			GestureRecognizers.Add (tgr);

		

		//	//Debug.WriteLine ("cè"  + "  "+ "  "+ nome  );
		

		}
		protected override void OnParentSet ()
		{
			base.OnParentSet ();



			DataSync ();
			////Debug.WriteLine ("cè"  + "  "+ "  "+ nome + this );
		}

	

	

		private static string is_unchecked { get { return "ui_empty.png"; } }

		public string pagina{ get; set; }

		public string nome{ get; set; }

		public string valore{ get; set; }

		public  string tipo{ get; set; }

		public string gruppo{ get; set; }

		public string txt{ get; set; }

		public string son{ get; set; }

		public int score{ get; set; }

		public int scoreB{ get; set; }

		private static string is_checked { get { return  "ui_check.png" ; } }
		private static string is_radio { get { return  "ui_radio.png" ; } }

		public void  DataSync ()
		{


			/*if (nome.Equals("s7_rd_1")){


				var valore = App.Survey.getRelationVal ();

				score = Int32.Parse(valore);

				//App.Survey.SetDataSurvey (nome, valore, "");

				//App.Survey.SetDataSurvey (nome, valore, "");




			}*/


		//	//Debug.WriteLine ("controllo "  + "  "+ App.Survey.GetSData.FindIndex(i => i.nome.Equals(nome)) );



			if (App.Survey.GetSData.FindIndex(i => i.nome.Equals(nome))>-1 ) {
			
				var cntrl = App.Survey.GetElByName (nome);
				//Debug.WriteLine ("cè");


				var b = false;


				if (cntrl.valore.Equals ("")) {

					b=false;



				} else {
				
					b= true;
				
				}







				//Page Cpagina = getPage ();

				SetStatus (this, b);



				var padre = App.Survey.GetParentBySon (nome);


				if (!padre.Equals("")){

					var datapadre = App.Survey.GetElByName (padre);
					 

					if (datapadre.valore.Equals("1")){

						IsVisible = true;

					}




				}



			
			} else {
			
				var txtEl =this.FindByName<Label> (nome + "_txt");



				var el = new Controls_front {
					nome = nome,
					valore = valore ,
					pagina = pagina ,
					tipo = tipo ,
					gruppo = gruppo,
					son = son ,
					score = score,
					txt=txt+": "+txtEl.Text,
					scoreB=scoreB
				
				};


				App.Survey.AddDataSurvey (el);

			
			}


			//Debug.WriteLine (JsonConvert.SerializeObject (App.Survey.GetSData));
			//Cpagina.Title = " [ " + App.Survey.GetTotScoreSurvey ().ToString () + " ] ";

		}



		public Page  getPage ()
		{
		
			var parent = Parent;
			Page pagina = new Page ();

			while (parent != null) {
				if (parent is Page) {

					pagina = parent as Page;
					////Debug.WriteLine ("tapped"+pagina.);
					break;


				}
				parent = parent.Parent;
			}


			return pagina;
		
		}

	
		public void SetStatus (Controls_me el, bool status)
		{



			var imgEl = el.FindByName<Image> (el.nome + "_img");

			var img = status ? el.tipo.Equals("check") ?is_checked: is_radio  : is_unchecked;

			imgEl.Source = img;


			el.valore = status ? "1" : "";



		

		
		}



		public void GetSons (Controls_me el, bool status)
		{



			Page Cpagina = getPage ();
			string[] figli = el.son.Split ( new char[] { ',' });

			foreach (var item in figli) {

				var ctrl =	Cpagina.FindByName<Controls_me> (item);
			
				var datastored = App.Survey.GetElByName (ctrl.nome);

				if (status) {

					ctrl.IsVisible = true;

				} else {
					ctrl.IsVisible = false;
					App.Survey.SetDataSurvey (ctrl.nome, "", "");

				}

				var is_checked = false;

				if (datastored != null) {
					if (datastored.valore.Equals ("1")) {

						is_checked = true;


					} 
				}
				SetStatus (ctrl, is_checked);



			}

		}




		public void GetSonsCk (Controls_me el, bool status)
		{



			Page Cpagina = getPage ();

	


			string[] figli = el.son.Split ( new char[] { ',' });


			var actived = figli.Length;

			var is_checked = false;

			foreach (var item in figli) {

				var ctrl =	Cpagina.FindByName<Controls_me> (item);

			

				if (status) {

					ctrl.IsVisible = true;
					//App.Survey.SetDataSurvey (ctrl.nome, "1", "");
					//is_checked = true;

				} else {
					actived--;
					ctrl.IsVisible = false;
					App.Survey.SetDataSurvey (ctrl.nome, "", "");
					is_checked = false;

				}

				SetStatus (ctrl, is_checked);





			}




		}


		public void OnLabelClicked ()
		{



			Page Cpagina = getPage ();


			//	var page= App.getCurrentPage;
			//this.pa

	

			switch (tipo) {

			case "radio":

				if (valore.Equals ("")) {

					var bro =	App.Survey.GetDataGroup (gruppo);
				

					if (bro.Length > 0) {

						foreach (var item in bro) {

							var ctrl =	Cpagina.FindByName<Controls_me> (item);


					
							if (ctrl != null) {

								if (ctrl.IsVisible != false) {


								
							
									var status = true;

									if (!item.Equals (nome)) {

										status = false;
						
									
									} else {
										
										//Debug.WriteLine ("on " + ctrl.nome + "  " + ctrl.valore);

										App.Survey.SetDataSurvey (nome, "1", gruppo);

									} 


									if (ctrl.son !=null ){

										GetSons (ctrl, status);

									}


									SetStatus (ctrl, status);

								}
							}

						}
					}


				}




				break;



			case "check":

				var active = false;

				var ck_data = App.Survey.GetElByName (nome);

				var ck =	Cpagina.FindByName<Controls_me> (nome);

				var val = "";

				if (ck_data.valore.Equals ("")) {


					active = true;
					val = "1";
					
				}

				App.Survey.SetDataSurvey (nome, val, "");


				if (ck.son !=null ){

					GetSonsCk (ck, active);

				}

				SetStatus (ck, active);




				//check if last child

				var padre = App.Survey.GetParentBySon (nome);


				if (!padre.Equals("")){

					//var datapadre = App.Survey.GetElByName (padre);
					var fratelli =	App.Survey.GetDataGroup (gruppo);
					var last = 0;

					foreach (var item in fratelli) {

						var ctrl =	Cpagina.FindByName<Controls_me> (item);

					

						if (ctrl.valore.Equals("1")){


							last++;
								
							}



					}

					if (last==0){

						App.Survey.SetDataSurvey (padre, "", "");

						SetStatus (Cpagina.FindByName<Controls_me> (padre), false);

						foreach (var item in fratelli) {

							var ctrl =	Cpagina.FindByName<Controls_me> (item);

							ctrl.IsVisible = false;
						}



					}





				}








				break;
			}










			//Cpagina.Title = " [ " + App.Survey.GetTotScoreSurvey ().ToString () + " ]   -   [ "  +  App.Survey.GetTotScoreBSurvey ().ToString ()  +  " ]";

		



		}




	}
}

