﻿using System;
using Xamarin.Forms;
using System.Collections;
using System.Diagnostics;

namespace ASM
{
	public class BindablePicker: Picker
	{

		public BindablePicker()
		{
			this.SelectedIndexChanged += OnSelectedIndexChanged;


			var ci = DependencyService.Get<ILocalize> ().GetCurrentCultureInfo ();

			if (ci.Name.IndexOf("it")>-1) {
				itemsLabel = itemsLabel_IT;

			} else {

				itemsLabel = itemsLabel_EN;
			}


		}





		public static readonly BindableProperty TextColorProperty = 
			BindableProperty.Create ("TextColor", typeof(Color), typeof(BindablePicker), Color.Default);

		public Color TextColor {
			get { return (Color)GetValue (TextColorProperty); }
			set { SetValue (TextColorProperty, value); }
		}

		public static readonly BindableProperty HasBorderProperty = 
			BindableProperty.Create ("HasBorder", typeof(bool), typeof(BindablePicker), true);

		public bool HasBorder {
			get { return (bool)GetValue (HasBorderProperty); }
			set { SetValue (HasBorderProperty, value); }
		}


		public string pagina{ get; set; }

		public string nome{ get; set; }

		public string valore{ get; set; }

		public  string tipo{ get; set; }

		public string gruppo{ get; set; }

		public string son{ get; set; }

		public string values{ get; set; }

		public string txt{ get; set; }

		public int score{ get; set; }

		public int scoreB{ get; set; }

		protected override void OnParentSet ()
		{
			base.OnParentSet ();
		


			Device.OnPlatform(
				Android: () =>{
					BackgroundColor=Color.FromHex("#ebebeb");


				

				}
			);




			DataSync ();

		}

		private string[] itemsLabel;

		private  string[] itemsLabel_IT= new string[] { "Master o Dottorato", "Laurea o Diploma universitario", "Diploma di scuola superiore", "Licenza media", "Licenza elementare" };
		private  string[] itemsLabel_EN= new string[] { "Master's, PhD", "Degree, university diploma", "High school diploma", "Middle school diploma", "Elementary school diploma" };


		public void  DataSync ()
		{


			foreach (var i in itemsLabel)
			{
				Items.Add(i);

			}





			if (App.Survey.GetSData.FindIndex (i => i.nome.Equals (nome)) > -1) {


				var cntrl = App.Survey.GetElByName (nome);


				if (!cntrl.valore.Equals ("")) {

					//var pos=	Array.IndexOf(itemsLabel, cntrl.valore);

					SelectedIndex = Int32.Parse(cntrl.son) ;

						//Int32.Parse(cntrl.valore);

				}


				
			} else {


				var el = new Controls_front {
					nome = nome,
					valore = valore ,
					pagina = pagina ,
					tipo = tipo ,
					gruppo = gruppo,
					son = son ,
					score = score,
					txt=txt,
					scoreB=scoreB
				};


				App.Survey.AddDataSurvey (el);

				
			}


		}

	

		private void OnSelectedIndexChanged(object sender, EventArgs eventArgs)
		{


			var pik = sender as BindablePicker;

			//Debug.WriteLine (">>> " + pik.SelectedIndex + "  "+ eventArgs + "  "+ values);


			App.Survey.SetDataSurvey (nome, pik.Items[pik.SelectedIndex] , "");


			string[] valori = values.Split ( new char[] { ',' });

			App.Survey.SetScoreSurvey (pik.nome ,Int32.Parse(valori[pik.SelectedIndex] ));


			App.Survey.SetSonSurvey(nome, pik.SelectedIndex.ToString() );

		/*	if (SelectedIndex < 0 || SelectedIndex > Items.Count - 1)
			{
				SelectedItem = null;
			}
			else
			{
				SelectedItem = Items[SelectedIndex];
			}*/
		}
		private static void OnSelectedItemChanged(BindableObject bindable, object oldvalue, object newvalue)
		{
			var picker = bindable as BindablePicker;
			if (newvalue != null)
			{
				picker.SelectedIndex = picker.Items.IndexOf(newvalue.ToString());
			}
		}

	}
}

