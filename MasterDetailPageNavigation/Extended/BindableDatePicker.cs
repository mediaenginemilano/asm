﻿using System;
using Xamarin.Forms;
using System.Diagnostics;

namespace ASM
{
	public class BindableDatePicker:DatePicker
	{
		public BindableDatePicker ()
		{

			Format = "D";
			Date = DateTime.Now;
		
		}



		public static readonly BindableProperty TextColorProperty = 
			BindableProperty.Create ("TextColor", typeof(Color), typeof(BindableDatePicker), Color.Default);

		public Color TextColor {
			get { return (Color)GetValue (TextColorProperty); }
			set { SetValue (TextColorProperty, value); }
		}

		public static readonly BindableProperty HasBorderProperty = 
			BindableProperty.Create ("HasBorder", typeof(bool), typeof(BindableDatePicker), true);

		public bool HasBorder {
			get { return (bool)GetValue (HasBorderProperty); }
			set { SetValue (HasBorderProperty, value); }
		}


		protected override void OnParentSet ()
		{
			base.OnParentSet ();
			MaximumDate = DateTime.Now;
			DataSync ();


		}



		public void  DataSync ()
		{


			if (App.Survey.GetSData.FindIndex (i => i.nome.Equals (nome)) > -1) {


				var cntrl = App.Survey.GetElByName (nome);


				if (!cntrl.valore.Equals ("")) {

					Date = DateTime.Parse ((cntrl.valore));

					//SelectedIndex = Int32.Parse(cntrl.valore);

				}



			} else {


				var el = new Controls_front {
					nome = nome,
					valore = valore ,
					pagina = pagina ,
					tipo = tipo ,
					gruppo = gruppo,
					son = son ,
					score = score,
					txt=txt,
					scoreB=scoreB,
					isReq=isReq

				};


				App.Survey.AddDataSurvey (el);


			}

			DateSelected += Change_Date;


		}


		public void Change_Date(object sender, DateChangedEventArgs e)
		{


			App.Survey.SetDataSurvey(nome, e.NewDate.Date.ToString("yyyy-MM-dd"), "");

			//Debug.WriteLine (sender +" " +e.NewDate.Date.ToString("yyyy-MM-dd"));

		}

		public string pagina{ get; set; }

		public string nome{ get; set; }

		public string valore{ get; set; }

		public  string tipo{ get; set; }

		public string gruppo{ get; set; }

		public string son{ get; set; }

		public string txt{ get; set; }
		public int scoreB{ get; set; }

		public string values{ get; set; }

		public int score{ get; set; }
		public int isReq{ get; set; }
	}
}

