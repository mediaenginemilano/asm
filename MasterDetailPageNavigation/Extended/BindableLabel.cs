﻿using System;
using Xamarin.Forms;
using System.Diagnostics;

namespace ASM
{
	public class BindableLabel:Entry
	{
		public BindableLabel ()
		{
		}


		protected override void OnParentSet ()
		{
			base.OnParentSet ();


			Completed += SetData;
			Unfocused += SetData;
			DataSync ();

			Device.OnPlatform(
				Android: () =>{
					BackgroundColor=Color.FromHex("#ebebeb");
					TextColor=Color.Black;
					PlaceholderColor=Color.Black;

				}
			);



		}

		public void  SetData (object sender, EventArgs eventArgs)
		{

			try {

			//	if (Text.Length > 0) {


					App.Survey.SetDataSurvey (nome, Text, "");

					//Debug.WriteLine ("---->>" + Text);
				//}

			} catch (NullReferenceException e) {
			
			
			}


		}

		public void  DataSync ()
		{


			if (App.Survey.GetSData.FindIndex (i => i.nome.Equals (nome)) > -1) {


				var cntrl = App.Survey.GetElByName (nome);


				if (!cntrl.valore.Equals ("")) {

					Text = cntrl.valore;

				}



			} else {


			

				var el = new Controls_front {
					nome = nome,
					valore = valore,
					pagina = pagina,
					tipo = tipo,
					gruppo = gruppo,
					son = son,
					score = score,
					txt=txt,
					scoreB=scoreB,
					isReq=isReq
				};


				App.Survey.AddDataSurvey (el);


			}




		}


		public Page  getPage ()
		{

			var parent = Parent;
			Page pagina = new Page ();

			while (parent != null) {
				if (parent is Page) {

					pagina = parent as Page;
					////Debug.WriteLine ("tapped"+pagina.);
					break;


				}
				parent = parent.Parent;
			}


			return pagina;

		}



		public string pagina{ get; set; }

		public string nome{ get; set; }

		public string valore{ get; set; }

		public  string tipo{ get; set; }

		public string gruppo{ get; set; }

		public string son{ get; set; }

		public string txt{ get; set; }

		public string values{ get; set; }

		public int score{ get; set; }

		public int scoreB{ get; set; }

		public int isReq{ get; set; }
	}
}

