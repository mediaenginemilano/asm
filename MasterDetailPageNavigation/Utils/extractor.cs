﻿using System;
using System.Collections.Generic;

namespace ASM
{
	public static  class extractor
	{
		public static List<string> ExtractFromString(string text, string startString, string endString)
		{            
			List<string> matched = new List<string>();
			int indexStart = 0, indexEnd=0;
			bool exit = false;
			while(!exit)
			{
				indexStart = text.IndexOf(startString);
				indexEnd = text.IndexOf(endString);
				if (indexStart != -1 && indexEnd != -1)
				{
					matched.Add(text.Substring(indexStart + startString.Length, 
						indexEnd - indexStart - startString.Length));
					text = text.Substring(indexEnd + endString.Length);
				}
				else
					exit = true;
			}
			return matched;
		}
	}
}

