﻿using Xamarin.Forms;
using System;

namespace ASM
{
	public partial class SurveyPage : ContentPage
	{
		public SurveyPage ()
		{
			InitializeComponent ();
		}


		public  void OpenSurvey(object sender, EventArgs e){


		

		

			if (App.Survey.checkFile) {





				Confirm ();



			} else {
				MessagingCenter.Send<SurveyPage> (this, "survey");
			
			}





		}



		public async void Confirm(){

		
			var answer = await DisplayAlert (Localize.GetString("PopupSurveyResetTitle","" ),Localize.GetString("PopupSurveyResetText","" ), "OK", "NO");

			if (answer) {

				App.Survey.DeleteResultFile ();
				MessagingCenter.Send<SurveyPage> (this, "survey");
			} 

		}


		public  void OpenResult(object sender, EventArgs e){


			MessagingCenter.Send<SurveyPage> (this, "OpenResult");
		}


		protected override void OnAppearing ()
		{
			base.OnAppearing ();

			if (App.Survey.checkFile) {
			
				btResult.IsVisible = true;
			
			} else {
			
				btResult.IsVisible = false;
			}



		}



	


	}
}

