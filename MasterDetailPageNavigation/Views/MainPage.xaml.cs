﻿using System;
using Xamarin.Forms;
using System.Collections.Generic;

namespace ASM
{
	public partial class MainPage : MasterDetailPage
	{

		public  Type corrente;

		public Color bgcolor;
		//public static Page getModalPage { get { return   Navigation.ModalStack;  } }

		public MainPage ()
		{

			if (Device.OS == TargetPlatform.Android) {

				bgcolor = Color.FromHex ("#45c064");


			}else{

				bgcolor = Color.White;

			}


			InitializeComponent ();



			IsPresentedChanged += OnPresentedChanged;

			masterPage.ListView.ItemSelected += OnItemSelected;

			masterPage.LangSel.Clicked += OnChangeLang;

			if (Device.OS == TargetPlatform.Windows) {
				Master.Icon = "swap.png";
			}



			Detail = new NavigationPage (new HomePage ()){


				BarBackgroundColor = bgcolor, 
				BarTextColor = Color.Black


			}; ;







			Initlistener ();

		}



	



		void Initlistener(){





			MessagingCenter.Subscribe<SurveyPage> (this, "OpenResult", (sender) => {
				
				Detail = new NavigationPage (new ResultPage_1 ()){ 


					BarBackgroundColor = bgcolor, 
					BarTextColor = Color.Black


				};


			



				IsPresented = false;


			});




			MessagingCenter.Subscribe<SurveyPage> (this, "survey", (sender) => {

				/*Detail = new NavigationPage (new step_1());
				IsPresented = false;*/

			
			

				Navigation.PushModalAsync( new NavigationPage( new  step_1()){ BarBackgroundColor =bgcolor, 
						BarTextColor = Color.Black


					});





			});
			MessagingCenter.Subscribe<HomePage> (this, "survey", (sender) => {


//				Navigation.PushModalAsync( new NavigationPage( new step_1()){ 
//					BarBackgroundColor = bgcolor, 
//					BarTextColor = Color.Black
//
//
//				});
				Detail = new NavigationPage (new SurveyPage ()) { BarBackgroundColor = bgcolor, 
					BarTextColor = Color.Black
				};


				IsPresented = false;
			});



			MessagingCenter.Subscribe<step_1,Dictionary<string, double>> (this, "bmi", (sender,arg) => {

				double  h = arg["h"];
				double  w= arg["w"];


				Navigation.PushModalAsync( new NavigationPage( new BmiPage_Modal(h,w)){ 
						BarBackgroundColor = bgcolor, 
						BarTextColor = Color.Black
					});
			

				IsPresented = false;
			});



			MessagingCenter.Subscribe<ResultPage_2> (this, "thankyou", (sender) => {

				Navigation.PushModalAsync( new NavigationPage( new ThankYou()){ 
					BarBackgroundColor = bgcolor, 
					BarTextColor = Color.Black
				});

			});


			MessagingCenter.Subscribe<ResultPage_2Modal> (this, "thankyou", (sender) => {

				Navigation.PushModalAsync( new NavigationPage( new ThankYou()){ 
					BarBackgroundColor = bgcolor, 
					BarTextColor = Color.Black
				});

			});



			MessagingCenter.Subscribe<step_24> (this, "result", (sender) => {
				Navigation.PopModalAsync();
				Detail = new NavigationPage (new ResultPage_1 ()) { BarBackgroundColor = bgcolor, 
					BarTextColor = Color.Black


				};
				IsPresented = false;

				App.ServiceManager.SendDataToWeb (App.Survey.GetSData);

			});
			MessagingCenter.Subscribe<ResultPage_1> (this, "result_1", (sender) => {
				Detail = new NavigationPage (new ResultPage_2 ()){ BarBackgroundColor = bgcolor, 
					BarTextColor = Color.Black


				};
				IsPresented = false;
			});
			MessagingCenter.Subscribe<ResultPage_2> (this, "result_2", (sender) => {
				Detail = new NavigationPage (new ResultPage_1 ()){ BarBackgroundColor = bgcolor, 
					BarTextColor = Color.Black


				};
				IsPresented = false;
			});
			MessagingCenter.Subscribe<ResultPage_1> (this, "resultModal_1", (sender) => {
				Navigation.PushModalAsync( new NavigationPage( new ResultPage_1Modal()){ BarBackgroundColor = bgcolor, 
					BarTextColor = Color.Black


				});
			});
			MessagingCenter.Subscribe<ResultPage_2> (this, "resultModal_2", (sender) => {
				Navigation.PushModalAsync( new NavigationPage( new ResultPage_2Modal()){ BarBackgroundColor = bgcolor, 
					BarTextColor = Color.Black


				});
			});


		}



		void OnPresentedChanged (object sender, EventArgs e)
		{

			System.Diagnostics.Debug.WriteLine("presente --- " + this.IsPresented);

		}


		void OnItemSelected (object sender, SelectedItemChangedEventArgs e)
		{


			var item = e.SelectedItem as MasterPageItem;


		


			if (item != null) {


				if(item.IconSource=="ico_heart.png"){

					corrente = item.TargetType;
					masterPage.ListView.SelectedItem = null;
					//IsPresented = false;

					Device.OpenUri(new Uri("http://www.asmonlus.it/Sostienici.aspx"));


					App.hitbox.dp=item.Title;


					App.ServiceManager.TrackPage (App.hitbox);
					return;
				}



				corrente = item.TargetType;


			
				
				Detail = new NavigationPage ((Page)Activator.CreateInstance (item.TargetType)){ BarBackgroundColor =bgcolor, 
						BarTextColor = Color.Black


					};
				




				masterPage.ListView.SelectedItem = null;
				IsPresented = false;


				App.hitbox.dp=item.Title;

				App.ServiceManager.TrackPage (App.hitbox);



			}
		}



	/*	public class NumericValidationTriggerAction : TriggerAction<ListView>
		{
			protected override void Invoke (ListView e)
			{

				//e = masterPage.ListView;

				System.Diagnostics.Debug.WriteLine("CLICK TRIGGER");


			}
		}*/




		void OnChangeLang (object sender, EventArgs e)
		{



			if (corrente == null){

				corrente = typeof(HomePage);

			}

		



			//IsPresented = false;

				//var item = corrente;
			if (corrente != null) {


				System.Diagnostics.Debug.WriteLine("CHECK"+App.forcedCulture.Equals ("en-US"));

				masterPage.FadeTo (0);


				var ci = DependencyService.Get<ILocalize> ().GetCurrentCultureInfo ();


				if (ci.Name.IndexOf ("en")>-1  ) {

					App.forcedCulture="it-IT";
					masterPage.LangSel.Text=Localize.GetString("cambiolinguaIT","" );

					masterPage.ListView.ItemsSource = masterPage.masterPageItemsIT;

				} else {

					App.forcedCulture="en-US";
					masterPage.LangSel.Text=Localize.GetString("cambiolinguaEN","" );
					masterPage.ListView.ItemsSource = masterPage.masterPageItems;
				}


				//DependencyService.Get<ILocalize> ().SetLocale ();


				masterPage.FadeTo (1);


				Detail = new NavigationPage ((Page)Activator.CreateInstance (corrente)){ BarBackgroundColor = bgcolor, 
					BarTextColor = Color.Black


				};
				masterPage.ListView.SelectedItem = null;
				IsPresented = false;

			

				//corrente = null;



			} else {

				System.Diagnostics.Debug.WriteLine("PRIMO");

			}



		}


	}
}
