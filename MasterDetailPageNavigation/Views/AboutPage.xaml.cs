﻿using System;
using System.Collections.Generic;

using Xamarin.Forms;
using System.Diagnostics;
using System.Text.RegularExpressions;

namespace ASM
{
	public partial class AboutPage : ContentPage
	{
		public AboutPage ()
		{
			InitializeComponent ();



		}


		protected override void OnAppearing ()
		{
			base.OnAppearing ();

			/*var tospan=	extractor.ExtractFromString (Localize.GetString ("supportcnt0", ""), "[", "]");

			var fs = new FormattedString ();

			foreach (string el in tospan ){


				fs.Spans.Add (new Span { Text=el , FontAttributes=FontAttributes.Bold });


			}*/

			var fs = new FormattedString ();


			var ci = DependencyService.Get<ILocalize> ().GetCurrentCultureInfo ();

			if (ci.Name.IndexOf ("it") > -1) {

				fs.Spans.Add (new Span {

					FontSize=15,
					Text = "La Fondazione ASM per la Salute dell’Infanzia affianca ASM, l’Associazione Italiana per lo Studio delle Malformazioni Onlus,",
					FontAttributes = FontAttributes.Bold
				});
				fs.Spans.Add (new Span { 	FontSize=15, Text = " con un obiettivo: informare le gestanti per prevenire i problemi di salute dei loro bambini. Gli stili di vita della futura mamma, e in particolare l’alimentazione, influenzano infatti in misura notevole la salute del nuovo nato nel corso della sua intera esistenza. Per questo, grazie al supporto di un Centro universitario accreditato come l’ICANS,"  });
				fs.Spans.Add (new Span {
					FontSize=15,
					Text = " la Fondazione ASM ha realizzato la prima APP dedicata esclusivamente alla corretta alimentazione per le donne che sono in attesa di un figlio.",
					FontAttributes = FontAttributes.Bold
				});
				fs.Spans.Add (new Span { 	FontSize=15,Text = " Ma è da 35 anni che, grazie ai contributi volontari di cittadini, aziende ed enti (le indicazioni per donare si trovano qui in fondo), "  });
				fs.Spans.Add (new Span {
					FontSize=15,
					Text = "ASM è al fianco delle donne nel momento più importante: quello in cui una nuova vita prende forma.",
					FontAttributes = FontAttributes.Bold
				});
				fs.Spans.Add (new Span { 	FontSize=15,Text = " E lo fa con iniziative ad ampia diffusione, come il"  });
				fs.Spans.Add (new Span { 	FontSize=15,Text = "“Decalogo per una maternità serena”", FontAttributes = FontAttributes.Bold });
				fs.Spans.Add (new Span { 	FontSize=15,Text = ", un compendio delle informazioni di base necessarie per vivere una gravidanza sicura, distribuito in quasi due milioni di copie. O come il "  });

				fs.Spans.Add (new Span {
					FontSize=15,
					Text = "Filo Rosso, ",
					FontAttributes = FontAttributes.Bold
				});

				fs.Spans.Add (new Span { 	FontSize=15,Text = "un servizio telefonico di consulenza medica gratuita sul rischio riproduttivo e sulla salute durante la gestazione offerto da specialisti in ostetricia, genetica e pediatria, attivo dal 1988 e oggi dotato di quattro postazioni in altrettanti ospedali pubblici italiani (telefoni ed orari sul sito www.asmonlus.it). Per divulgare le conoscenze mediche e scientifiche più avanzate in tema di gravidanza, ASM pubblica il periodico "  });



				fs.Spans.Add (new Span {	FontSize=15, Text = "“Educazione alla Salute”", FontAttributes = FontAttributes.Bold });
				fs.Spans.Add (new Span { 	FontSize=15,Text = " , che si occupa anche di malattie congenite, un flagello silenzioso che ancor oggi, in Italia, colpisce ogni anno 28mila bambini, uno ogni 18 minuti circa. E poi c’è la prevenzione attraverso la diagnosi precoce: ASM acquista "  });
				fs.Spans.Add (new Span {
					FontSize=15,
					Text = "apparecchiature tecnologicamente all’avanguardia",
					FontAttributes = FontAttributes.Bold
				});
				fs.Spans.Add (new Span {	FontSize=15, Text = ", da destinare ad ospedali pubblici, per ecografie ed esami prenatali, allo scopo di individuare più precocemente e con maggiore precisione le eventuali anomalie presenti nel feto, permettendo così ai medici di intervenire con le opportune terapie durante e dopo la gestazione. Nel settore della ricerca, ASM sostiene il "  });
				fs.Spans.Add (new Span { 	FontSize=15,Text = "Centro di Ricerche Fetali “Giorgio Pardi”", FontAttributes = FontAttributes.Bold });
				fs.Spans.Add (new Span {	FontSize=15, Text = ", che si propone di studiare i meccanismi alla base delle patologie fetali e di mettere a punto terapie avanzate per la prevenzione delle malattie della gravidanza. Per quanto riguarda la fase della cura, "  });

				fs.Spans.Add (new Span {
					FontSize=15,
					Text = "ASM ha donato cinque incubatrici da trasporto di ultima generazione per la terapia intensiva dei neonati prematuri ",
					FontAttributes = FontAttributes.Bold
				});

				fs.Spans.Add (new Span { 	FontSize=15,Text = "ad altrettanti ospedali in tutta Italia. E ha attivato dal 2007 un "  });
				fs.Spans.Add (new Span {
					FontSize=15,
					Text = "Centro polispecialistico per la diagnosi e la cura delle Malformazioni Vascolari congenite",
					FontAttributes = FontAttributes.Bold
				});
				fs.Spans.Add (new Span { 	FontSize=15,Text = " dotato di altissime professionalità mediche e di tecnologie laser molto sofisticate."  });

			} else {

				fs.Spans.Add (new Span {
					FontSize=15,
					Text = "The Fondazione ASM per la Salute dell’Infanzia collaborates with ASM, the Associazione Italiana per lo Studio delle MalformazioniOnlus, " ,
					FontAttributes = FontAttributes.Bold
				});
				fs.Spans.Add (new Span {	FontSize=15, Text = "with the aim of providing pregnant women with information in order to prevent health problems in their children. The expectant mother’s lifestyle, and diet in particular, considerably affects the health of newborns throughout their life. For this reason Fondazione ASM, with the support of the accredited university center ICANS, "  });
				fs.Spans.Add (new Span {
					FontSize=15,
					Text = "developed the first APP entirely dedicated to a correct diet for expectant mothers. " ,
					FontAttributes = FontAttributes.Bold
				});
				fs.Spans.Add (new Span {	FontSize=15, Text = "For 35 years ASM, thanks to donations from citizens, companies and organizations (instructions on how to donate are given below) "  });
				fs.Spans.Add (new Span {
					FontSize=15,
					Text = "has supported women during this most important time – the forming of a new life. " ,
					FontAttributes = FontAttributes.Bold
				});
				fs.Spans.Add (new Span { 	FontSize=15,Text = "This it does through wide-reaching initiatives such as "  });
				fs.Spans.Add (new Span { 	FontSize=15,Text = "the Decalogue for Serene Motherhood, ", FontAttributes = FontAttributes.Bold });
				fs.Spans.Add (new Span { 	FontSize=15,Text = "a compendium of medical information necessary for a healthy pregnancy, with nearly two million copies distributed. Another is Filo Rosso, a hotline offering free medical consulting on reproductive risks and health during pregnancy, provided by specialists in obstetrics, genetics, and pediatrics.Founded in 1988, today Filo Rosso has four units in as many Italian public hospitals (for telephone numbers and office hours, please visit www.asmonlus.it). "  });
				fs.Spans.Add (new Span { 	FontSize=15,Text = "In order to publicly  disseminate the latest medical and scientific knowledge on pregnancy ASM publishes “Educazionealla salute”, a magazine that also deals with congenital disorders, a silent plague that still affects 28 thousand children every year in Italy, approximately one every 18 minutes. "  });
				fs.Spans.Add (new Span {
					FontSize=15,
					Text = "Another focus is prevention through early diagnosis: ASM purchases technologically advanced equipment" ,
					FontAttributes = FontAttributes.Bold
				});
				fs.Spans.Add (new Span { 	FontSize=15,Text = "for public hospitals to perform ultrasound scans and prenatal examinations for detecting fetal anomalies more quickly and accurately, thus enabling physicians to intervene with the proper therapies during and after pregnancy. "  });
				fs.Spans.Add (new Span {
					FontSize=15,
					Text = "In the field of research, ASM supports the “Giorgio Pardi”" ,
					FontAttributes = FontAttributes.Bold
				});
				fs.Spans.Add (new Span { 	FontSize=15,Text = " Fetal Research Center, which studies the mechanisms underlying fetal pathologies and develops advanced treatments for the prevention of pregnancy-related illnesses. "  });
				fs.Spans.Add (new Span { 	FontSize=15,Text = "As far as care is concerned, ASM donated five state-of-the-art portable intensive care incubators for premature babies to five hospitals throughout Italy. "  });
				fs.Spans.Add (new Span {
					FontSize=15,
					Text = "And in 2007 ASM instituted a Multi-specialty Center for the diagnosis and treatment of congenital vascular malformations, " ,
					FontAttributes = FontAttributes.Bold
				});
				fs.Spans.Add (new Span { 	FontSize=15,Text = "endowed with the highest medical expertise and advanced laser technologies."  });

			}



			   
				

			cnt_1.FormattedText = fs;

		}






	}
}

