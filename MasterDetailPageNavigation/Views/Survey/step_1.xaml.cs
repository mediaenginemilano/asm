﻿using System;
using System.Collections.Generic;

using Xamarin.Forms;
using System.Diagnostics;

namespace ASM
{
	public partial class step_1 : ContentPage
	{
		public step_1 ()
		{
			InitializeComponent ();
		}
		protected override void OnAppearing ()
		{
			base.OnAppearing ();
			App.hitbox.dp="Survey step 1";
			App.ServiceManager.TrackPage (App.hitbox);

		//	img.WidthRequest = 750/0.5859375;
		}
		public async  void Chiudi(object sender, EventArgs e){
			await	Navigation.PopModalAsync();
		}
		void GoResultBMI(object sender, EventArgs e){
			double n;

			bool height  = double.TryParse(s1_lb_2.Text, out n) ;

			bool weight = double.TryParse (s1_lb_3.Text, out n);

			if ( height &&  weight){


				var h=  Convert.ToDouble( s1_lb_2.Text) ;
				var w= Convert.ToDouble(s1_lb_3.Text);

				Dictionary<string, double> bmi = new Dictionary<string, double>();
				bmi.Add ("h", h);
				bmi.Add ("w", w);
				MessagingCenter.Send<step_1,Dictionary<string, double>> (this, "bmi",bmi);
			}

		}
		public async void GoNext(object sender, EventArgs e){
		
			//await Navigation.PushAsync(  new ResultPage_1() );

			//App.Survey.GetIsPageCompleted ("Step_1");

			if (!App.Survey.GetIsPageCompleted ("Step_1")) {


				await DisplayAlert (Localize.GetString("validatoreTit","" ), Localize.GetString("validatoreTxt","" ), Localize.GetString("validatoreBt","" ) );
				//await DisplayAlert ("Alcuni campi non sono stati compilati", "Per procedere occorre rispondere a tutte le domande", "ok");
			} else {


				await Navigation.PushAsync (new step_2 ());

			
			}





		//	App.ServiceManager.SendDataToWeb (App.Survey.GetSData);
		

		}
	
	}
}

