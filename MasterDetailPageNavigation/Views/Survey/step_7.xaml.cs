﻿using System;
using System.Collections.Generic;

using Xamarin.Forms;
using System.Diagnostics;

namespace ASM
{
	public partial class step_7 : ContentPage
	{
		public step_7 ()
		{
			InitializeComponent ();
		}

		protected override void OnAppearing ()
		{
			base.OnAppearing ();
			App.hitbox.dp="Survey step 7";
			App.ServiceManager.TrackPage (App.hitbox);


			/*var c= s7_rd_1_img.Source as FileImageSource;

			if (c.File.IndexOf("empty")==-1 ){

				if (App.Survey.getRelationVal ()=="1" ){

					App.Survey.SetDataSurvey ("s7_rd_1", "1", "");
				}

				Debug.WriteLine ("XXXX  " );
			}
		*/


		}

		public async  void Chiudi(object sender, EventArgs e){
			await	Navigation.PopModalAsync();
		}

		public async void GoNext(object sender, EventArgs e){
			if (!App.Survey.GetIsPageCompleted ("Step_7")) {


				await DisplayAlert (Localize.GetString("validatoreTit","" ), Localize.GetString("validatoreTxt","" ), Localize.GetString("validatoreBt","" ) );
				//await DisplayAlert ("Alcuni campi non sono stati compilati", "Per procedere occorre rispondere a tutte le domande", "ok");
			} else {


				await Navigation.PushAsync(  new step_8() );


			}
		}


	}
}

