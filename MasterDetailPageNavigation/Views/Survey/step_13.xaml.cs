﻿using System;
using System.Collections.Generic;

using Xamarin.Forms;

namespace ASM
{
	public partial class step_13 : ContentPage
	{
		public step_13 ()
		{
			InitializeComponent ();
		}

		protected override void OnAppearing ()
		{
			base.OnAppearing ();
			App.hitbox.dp="Survey step 13";
			App.ServiceManager.TrackPage (App.hitbox);
		}

		public async  void Chiudi(object sender, EventArgs e){
			await	Navigation.PopModalAsync();
		}

		public async void GoNext(object sender, EventArgs e){
			if (!App.Survey.GetIsPageCompleted ("Step_13")) {


				await DisplayAlert (Localize.GetString("validatoreTit","" ), Localize.GetString("validatoreTxt","" ), Localize.GetString("validatoreBt","" ) );
				//await DisplayAlert ("Alcuni campi non sono stati compilati", "Per procedere occorre rispondere a tutte le domande", "ok");
			} else {


				await Navigation.PushAsync(  new step_14() );


			}
		}


	}
}

