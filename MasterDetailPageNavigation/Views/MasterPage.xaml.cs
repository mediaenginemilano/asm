﻿using System.Collections.Generic;
using Xamarin.Forms;
using System;

namespace ASM
{
	public partial class MasterPage : ContentPage
	{
		public ListView ListView { get { return listView; } }

		public  Button  LangSel { get { return LangSelector; } }



		public List<MasterPageItem> masterPageItems;

		public List<MasterPageItem> masterPageItemsIT;


		public MasterPage ()
		{
			InitializeComponent ();
			this.BackgroundColor = Color.FromHex ("#eeeeee");

			masterPageItems = new List<MasterPageItem> ();
			masterPageItems.Add (new MasterPageItem {
				Title = " ",
				IconSource = "logo_nav.png",
				TargetType = typeof(HomePage)
			});
			masterPageItems.Add (new MasterPageItem {
				Title = "    About us",
				IconSource = "ico_contact.png",
				TargetType = typeof(AboutPage)
			});
			masterPageItems.Add (new MasterPageItem {
				Title = "    Survey",
				IconSource = "ico_survey.png",
				TargetType = typeof(SurveyPage)
			});
			masterPageItems.Add (new MasterPageItem {
				Title = "    News",
				IconSource = "ico_news.png",
				TargetType = typeof(NewsPage)
			});
			masterPageItems.Add (new MasterPageItem {
				Title = "     Recipes",
				IconSource = "ico_recipes.png",
				TargetType = typeof(RecipesPage)
			});
			masterPageItems.Add (new MasterPageItem {
				Title = "    Support ASM",
				IconSource = "ico_heart.png",
				TargetType = typeof(NewsPage)
			});

			masterPageItems.Add (new MasterPageItem {
				Title = "    keep in touch",
				IconSource = "ico_upd.png",
				TargetType = typeof(SupportPage)
			});


			masterPageItems.Add (new MasterPageItem {
				Title = "    Legal",
				IconSource = "ico_legal.png",
				TargetType = typeof(LegalPage)
			});


			masterPageItemsIT = new List<MasterPageItem> ();
			masterPageItemsIT.Add (new MasterPageItem {
				Title = " ",
				IconSource = "logo_nav.png",
				TargetType = typeof(HomePage)
			});
			masterPageItemsIT.Add (new MasterPageItem {
				Title = "    Chi siamo",
				IconSource = "ico_contact.png",
				TargetType = typeof(AboutPage)
			});
			masterPageItemsIT.Add (new MasterPageItem {
				Title = "    Questionario",
				IconSource = "ico_survey.png",
				TargetType = typeof(SurveyPage)
			});
			masterPageItemsIT.Add (new MasterPageItem {
				Title = "    News",
				IconSource = "ico_news.png",
				TargetType = typeof(NewsPage)
			});
			masterPageItemsIT.Add (new MasterPageItem {
				Title = "     Ricette",
				IconSource = "ico_recipes.png",
				TargetType = typeof(RecipesPage)
			});
			masterPageItemsIT.Add (new MasterPageItem {
				Title = "    Sostieni ASM",
				IconSource = "ico_heart.png",
				TargetType = typeof(HomePage)
			});


			masterPageItemsIT.Add (new MasterPageItem {
				Title = "    Restiamo in contatto",
				IconSource = "ico_upd.png",
				TargetType = typeof(SupportPage)
			});



			masterPageItemsIT.Add (new MasterPageItem {
				Title = "    Legal",
				IconSource = "ico_legal.png",
				TargetType = typeof(LegalPage)
			});
		}


		protected override void OnAppearing ()
		{
			base.OnAppearing ();




		
			var ci = DependencyService.Get<ILocalize> ().GetCurrentCultureInfo ();

			if (ci.Name.IndexOf("it")>-1) {
				listView.ItemsSource = masterPageItemsIT;

				//App.forcedCulture="it-IT";
				LangSel.Text=Localize.GetString("cambiolinguaIT","" );

			



			} else {

				listView.ItemsSource = masterPageItems;
				//App.forcedCulture="en-US";
				LangSel.Text=Localize.GetString("cambiolinguaEN","" );
			}



			if (Device.OS == TargetPlatform.Android) {
				LangSel.IsVisible = false;
			}


		}




	}
}
