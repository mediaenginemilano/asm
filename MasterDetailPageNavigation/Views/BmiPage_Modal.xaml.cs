﻿using System;
using System.Collections.Generic;

using Xamarin.Forms;

namespace ASM
{
	public partial class BmiPage_Modal : ContentPage
	{

		public double  height;
		public double  weight;

		public BmiPage_Modal (double h , double w)
		{
			InitializeComponent ();

			height = h/100;
			weight = w;
		}
		public async  void Chiudi(object sender, EventArgs e){
			await Navigation.PopModalAsync ();
		}

		protected override void OnAppearing ()
		{
			base.OnAppearing ();
			bmi.Text =  Math.Round( weight/ (height * height),2).ToString(); 
		}

	}
}

