﻿using System;
using System.Collections.Generic;

using Xamarin.Forms;
using System.Diagnostics;

namespace ASM
{
	public partial class ResultPage_1 : ContentPage
	{

		private int totale =0;

		private string immagine="";
		private string colore="";
		private string testo="";


		public int cc=0;


		public ResultPage_1 ()
		{
			InitializeComponent ();

			totale=App.Survey.GetTotScoreSurvey ();

		/*	var tapGestureRecognizer = new TapGestureRecognizer();
			tapGestureRecognizer.Tapped += (s, e) => {

				var img = s as Image;


				var st = "grp1_";

				if (cc<10){

					st+="0";
				}

				img.Source = st + cc.ToString ();



				cc++;


				// handle the tap
			};
			immagine_r1.GestureRecognizers.Add(tapGestureRecognizer);

*/


		}
		void GoResult(object sender, EventArgs e){
			MessagingCenter.Send<ResultPage_1> (this, "result_1");
		}
		void GoResultModal(object sender, EventArgs e){
			MessagingCenter.Send<ResultPage_1> (this, "resultModal_1");
		}






		protected override void OnAppearing ()
		{
			base.OnAppearing ();

			App.Survey.SaveResultFile ();

			//Debug.WriteLine (totale);



		//	totale = 10;

			var st = "grp1_";

			if (totale<10){

				st+="0";
			}



			if (totale<= 6){

				colore="#d33c3c";
				testo = "SurveyResultLabel01LowRange";

			}else if(totale>= 7 && totale<=8 ){

				colore="#ffd900";
				testo = "SurveyResultLabel01MediumRange";

			}else{

				colore="#84dd11";
				testo = "SurveyResultLabel01HighRange";
			}




			if (totale>14){

				totale = 14;
			}


		
			//this.FindByName

			for (var i=0 ; i<=14 ;i++){


				var stack=	this.FindByName<StackLayout> ("r_"+i);
				var txt = stack.Children [0] as Label;

				var bg = "#FFFFFF";



				if (totale== i ){

					bg=colore;
					txt.TextColor = Color.White;

				}

				stack.BackgroundColor = Color.FromHex (bg);
			}

			/*var imgplace = totale >= 2 ? (totale - 1) : totale;


			if (imgplace<10){

				st="grp1_0";
			}
*/

			var imgplace = totale.ToString ();

			immagine = st + imgplace + ".png";

			immagine_r1.Source = immagine;


			//Debug.WriteLine (totale +" --- "+immagine);

			titolazione.Text= Localize.GetString(testo,"" );


		}
	}
}

