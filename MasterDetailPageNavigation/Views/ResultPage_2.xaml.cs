﻿using System;
using System.Collections.Generic;

using Xamarin.Forms;
using System.Diagnostics;

namespace ASM
{
	public partial class ResultPage_2 : ContentPage
	{

		private int totale =0;

		private string immagine="";
		private string colore="";
		private string testo="";


		public ResultPage_2 ()
		{
			totale=App.Survey.GetTotScoreBSurvey ();
			InitializeComponent ();
		}
		void GoResult(object sender, EventArgs e){
			MessagingCenter.Send<ResultPage_2> (this, "result_2");
		}
		void GoModal(object sender, EventArgs e){
			MessagingCenter.Send<ResultPage_2> (this, "resultModal_2");
		}


		void GoTank(object sender, EventArgs e){
			MessagingCenter.Send<ResultPage_2> (this, "thankyou");
		}

		protected override void OnAppearing ()
		{
			base.OnAppearing ();



			//totale = 0;

			var st = "grp2_";

			if (totale<10){

				st+="0";
			}



			if (totale<=1){

				colore="#d33c3c";
				testo = "SurveyResultLabel02LowRange";

			}else if(totale>= 2 && totale<=4 ){

				colore="#ffd900";
				testo = "SurveyResultLabel02MediumRange";

			}else{

				colore="#84dd11";
				testo = "SurveyResultLabel02HighRange";
			}


		
			if (totale>=7){

				totale = 6;
			}




			//this.FindByName

			for (var i=1 ; i<=7 ;i++){


				var stack=	this.FindByName<StackLayout> ("r2_"+i);
				var txt = stack.Children [0] as Label;

				var bg = "#FFFFFF";


			

				if (totale+1==i){

					bg=colore;
					txt.TextColor = Color.White;

				}

				stack.BackgroundColor = Color.FromHex (bg);
			}

			immagine = st + totale + ".png";

			immagine_r2.Source = immagine;

			titolazione_2.Text= Localize.GetString(testo,"" );

			//Debug.WriteLine (totale +" --- "+immagine);


		}

	}
}

