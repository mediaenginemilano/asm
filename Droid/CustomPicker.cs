﻿using System;
using Xamarin.Forms;
using ASM.Droid;
using ASM;
using Android.Views;
using Xamarin.Forms.Platform.Android;
using Android.App;
using Android.Graphics.Drawables;
using Android.Text;
using Android.Text.Style;

[assembly:ExportRenderer (typeof(BindablePicker), typeof(CustomPicker))]
namespace ASM.Droid
{
	public class CustomPicker: PickerRenderer 
	{
		protected override void OnElementChanged(ElementChangedEventArgs<Picker> e)
		{
			base.OnElementChanged(e);

			BindablePicker cPicker = (BindablePicker)Element;

			if (cPicker != null)
			{
				//SetTextColor(datePicker);


				this.Control.SetTextColor (Android.Graphics.Color.Black);
				this.Control.SetHintTextColor(Android.Graphics.Color.Black);
			}

			if (e.OldElement == null)
			{
				//Wire events
			}

			if (e.NewElement == null)
			{
				//Unwire events
			}
		}

	
	}
}

