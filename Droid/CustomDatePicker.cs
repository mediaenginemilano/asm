﻿using System;
using Xamarin.Forms;
using ASM.Droid;
using ASM;
using Android.Views;
using Xamarin.Forms.Platform.Android;
using Android.App;
using Android.Graphics.Drawables;
using Android.Text;
using Android.Text.Style;

[assembly:ExportRenderer (typeof(BindableDatePicker), typeof(CustomDatePicker))]
namespace ASM.Droid
{
	public class CustomDatePicker: DatePickerRenderer 
	{
		protected override void OnElementChanged(ElementChangedEventArgs<DatePicker> e)
		{
			base.OnElementChanged(e);

			BindableDatePicker datePicker = (BindableDatePicker)Element;

			if (datePicker != null)
			{
				


				this.Control.SetTextColor (Android.Graphics.Color.Black);
				this.Control.SetHintTextColor(Android.Graphics.Color.Black);
			}

			if (e.OldElement == null)
			{
				//Wire events
			}

			if (e.NewElement == null)
			{
				//Unwire events
			}
		}

	
	}
}

