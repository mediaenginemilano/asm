﻿using System;
using System.Collections.Generic;
using System.Linq;

using Foundation;
using UIKit;
using System.Threading;

namespace ASM.iOS
{
	[Register ("AppDelegate")]
	public partial class AppDelegate : global::Xamarin.Forms.Platform.iOS.FormsApplicationDelegate
	{
		public override bool FinishedLaunching (UIApplication app, NSDictionary options)
		{
			global::Xamarin.Forms.Forms.Init ();

			#region Localization Debug info
			foreach (var s in NSLocale.PreferredLanguages) {
				Console.WriteLine ("pref:" + s);
			}

			var iosLocaleAuto = NSLocale.AutoUpdatingCurrentLocale.LocaleIdentifier;
			var iosLanguageAuto = NSLocale.AutoUpdatingCurrentLocale.LanguageCode;
			Console.WriteLine ("nslocaleid:" + iosLocaleAuto);
			Console.WriteLine ("nslanguage:" + iosLanguageAuto);


			var iosLocale = NSLocale.CurrentLocale.LocaleIdentifier;
			var iosLanguage = NSLocale.CurrentLocale.LanguageCode;
			var netLocale = iosLocale.Replace ("_", "-");
			var netLanguage = iosLanguage.Replace ("_", "-");
			Console.WriteLine ("ios:" + iosLanguage + " " + iosLocale);
			Console.WriteLine ("net:" + netLanguage + " " +  netLocale);

			Console.WriteLine ("culture:" + Thread.CurrentThread.CurrentCulture);
			Console.WriteLine ("uiculture:" + Thread.CurrentThread.CurrentUICulture);
			#endregion


			//UIApplication.SharedApplication.SetStatusBarStyle(UIStatusBarStyle.LightContent, false);
			//UIApplication.SharedApplication.SetStatusBarHidden(false, false);


			#if GORILLA
			LoadApplication (UXDivers.Artina.Player.iOS.Player.CreateApplication(new UXDivers.Artina.Player.Config("Good Gorilla"))); 
			#else
			LoadApplication (new App ());
			#endif

			//LoadApplication (new App ());
			//app.StatusBarStyle = UIStatusBarStyle.BlackOpaque;
			return base.FinishedLaunching (app, options);
		}
	}
}

